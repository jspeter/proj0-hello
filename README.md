#Proj0-Hello
-------------

Software that prints a message that is set inside of the credentials.ini file.

##Usage
-------------
```bash
make run # returns message contained within credentials.ini
```

##Authors 
------------
Jake Petersen jpeter17@uoregon.edu
